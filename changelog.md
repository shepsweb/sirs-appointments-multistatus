# Change Log

## 2.2.4 - 2016-12-09
* Added getPastDueAttribute to Appointment.  returns true if the appointment starts_at < now and status is pending (1);