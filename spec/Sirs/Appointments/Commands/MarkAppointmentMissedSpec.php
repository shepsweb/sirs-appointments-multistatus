<?php

namespace spec\Sirs\Appointments\Commands;

use Sirs\Appointments\Appointment;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

class MarkAppointmentMissedSpec extends ObjectBehavior
{
    function it_is_initializable()
    {
        $this->beConstructedWith(new Appointment());
        $this->shouldHaveType('Sirs\Appointments\Commands\MarkAppointmentMissed');
    }
}
