<?php

namespace spec\Sirs\Appointments\Commands;

use Illuminate\Database\Eloquent\Model as Eloquent;
use Sirs\Appointments\Appointment;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

class ScheduleAppointmentSpec extends ObjectBehavior
{
    function it_is_initializable()
    {
      $this->beConstructedWith(1, 'App\\Participant', 1, '2015-01-01', '12:00:00');
      $this->shouldHaveType('Sirs\Appointments\Commands\ScheduleAppointment');
    }

}
