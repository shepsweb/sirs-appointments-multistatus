<?php

namespace spec\Sirs\Appointments\Commands;

use Sirs\Appointments\Appointment;
use Sirs\Appointments\AppointmentStatus;

use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

class CancelAppointmentSpec extends ObjectBehavior
{
    function let(Appointment $appointment){
      $appointment->beADoubleOf('\Sirs\Appointments\Contracts\Appointment');
      $this->beConstructedWith($appointment);
    }

    function it_is_initializable()
    {
        $this->shouldHaveType('Sirs\Appointments\Commands\CancelAppointment');
    }

    function it_should_set_the_appointment_status_to_canceled($appointment)
    {
      // $this->appointment->appointment_status_id->shouldBeEqual(AppointmentStatus::findBySlug('canceled')->id);

      // $this->handle();
    }
}
