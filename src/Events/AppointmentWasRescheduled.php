<?php namespace Sirs\Appointments\Events;

use Sirs\Appointments\Contracts\Appointment;

use Illuminate\Queue\SerializesModels;

class AppointmentWasRescheduled extends AppointmentEvent {

  use SerializesModels;

  public $appointment;

  /**
   * Create a new event instance.
   *
   * @param  Appointment $appointment appointment that status was updated on
   * @return void
   */
  public function __construct(Appointment $appointment)
  {
    $this->appointment = $appointment;
  }

}