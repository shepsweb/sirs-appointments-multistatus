<?php namespace Sirs\Appointments\Events;

use Sirs\Appointments\Contracts\Appointment;
use Sirs\Appointments\Events\AppointmentEvent;

use Illuminate\Queue\SerializesModels;

class AppointmentScheduled extends AppointmentEvent {

	use SerializesModels;
	
	var $appointment;

	/**
	 * Create a new event instance.
	 *
	 * @return void
	 */
	public function __construct(Appointment $appointment)
	{
		$this->appointment = $appointment;
	}

}
