<?php 
namespace Sirs\Appointments\Controllers;

use Sirs\Appointments\Requests;
use Sirs\Appointments\Controllers\Controller;
use Sirs\Appointments\Requests\AppointmentRequest;
use Sirs\Appointments\Requests\AppointmentRescheduleRequest;

use Sirs\Appointments\Contracts\Appointment;
use Sirs\Appointments\Contracts\AppointmentType;
use Sirs\Appointments\Contracts\AppointmentStatus;
use Sirs\Appointments\Commands\ScheduleAppointment;
use Sirs\Appointments\Commands\CancelAppointment;
use Sirs\Appointments\Commands\RescheduleAppointment;
use Sirs\Appointments\Commands\markAppointmentAttended;
use Sirs\Appointments\Commands\markAppointmentMissed;


use Illuminate\Http\Request;
use Response;
use Carbon\Carbon;
use Illuminate\Contracts\Bus\Dispatcher;
use Illuminate\Database\Eloquent\Collection;
use Redirect;
use Validator;

class AppointmentStatusesController extends Controller {

  protected $validFilters = [
    'id',
    'name',
    'slug',
  ];

  /**
   * Display a listing of the resource.
   *
   * @return Response
   */
  public function index(Request $request)
  {
    $query = class_appointmentStatus()::query();
    foreach( $request->all() as $field=>$value ){
      if( in_array($field, $this->validFilters) ) 
        $query->where($field,'=',$value);
    }
    $count = $query->count();

    if( $limit = $request->query('limit') ){
      $query->take($limit);
    }
    if( $offset = $request->query('offset') ){
      $query->skip($offset);
    }

    $items = $query->get();

    return Response::json([
      'total' => $count,
      'items' => $items
    ], 200);
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return Response
   */
  public function show(Appointment $appointment)
  {
    $item = $this->transform($appointment);
    return Response::json([
      'item'=>$item
    ]);
  }


  protected function transformCollection(Collection $appointmentTypes)
  {
    return $appointmentTypes->map(function (AppointmentType $appointmentType) {
      return $this->transform($appointment);
    });
  }

  protected function transform (AppointmentType $appointmentType)
  {
    return [
      'id'=>$appointmentType->id,
      'name'=>$appointmentType->name,
      'slug'=>$appointmentType->slug,
    ];
  }


}
